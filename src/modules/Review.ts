export class ConsOrPro {
    conOrPro: boolean
    description: string
}

export class Review {
    _id: string
    title: string
    description: string
    rating: number
    consOrPros: ConsOrPro[]

    constructor(values = {}) {
        Object.assign(this, values)
    }
}
