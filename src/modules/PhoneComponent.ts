export class PhoneComponent {
    _id: string
    type: string
    description: string

    constructor(values = {}) {
        Object.assign(this, values)
    }
}
