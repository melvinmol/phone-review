import { PhoneComponent } from '../modules/PhoneComponent'
import { Review } from './Review'

export class Phone {
    _id: string
    name: string
    company: string
    price: number
    components: PhoneComponent[]
    reviews: Review[]

    constructor(values = {}) {
        Object.assign(this, values)
    }
}
