import { Observable, from } from 'rxjs'
import { map, reduce } from 'rxjs/operators'

export enum AccountRole {
    Basic = 0,
    Reviewer = 1,
    Admin = 2
}

export class Account {
    name: string
    email: string
    role: AccountRole
    password: string
    token: string

    roles: AccountRole[]

    constructor(values = {}) {
        Object.assign(this, values)
        this.roles = [this.role]
    }

    public hasRole(rolename: AccountRole): Observable<boolean> {
        return from(this.roles).pipe(
            map(val => val === rolename),
            reduce((a, b) => a || b)
        )
    }
}
