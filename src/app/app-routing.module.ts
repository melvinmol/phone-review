import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { LoginComponent } from './account/login/login.component'
import { PhoneComponent } from './phone/phone.component'
import { AddAccountComponent } from './account/add-account/add-account.component'

const routes: Routes = [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'about', component: UsecasesComponent },
    { path: 'login', component: LoginComponent },
    { path: 'account/add', component: AddAccountComponent },
    { path: 'phone', component: PhoneComponent },
    { path: '**', redirectTo: '/' }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
