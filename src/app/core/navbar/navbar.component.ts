import { Component, Input, OnInit, OnDestroy } from '@angular/core'
import { Observable, Subscription } from 'rxjs'
import { AuthenticationService } from 'src/app/account/authentication.service'
import { Router } from '@angular/router'

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
    @Input() title: string
    isLoggedIn$: Observable<boolean>
    isAdmin$: Observable<boolean>
    fullNameSubscription: Subscription
    fullName = ''

    isNavbarCollapsed = true

    constructor(private authenticationService: AuthenticationService, private router: Router) {}

    ngOnInit() {
        this.isLoggedIn$ = this.authenticationService.accountIsLoggedIn
        this.isAdmin$ = this.authenticationService.accountIsAdmin
        this.fullNameSubscription = this.authenticationService.accountFullName.subscribe(
            name => (this.fullName = name)
        )
    }

    onLogout() {
        this.authenticationService.logout()
    }

    ngOnDestroy() {
        this.fullNameSubscription.unsubscribe()
    }
}
