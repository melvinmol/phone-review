import { TestBed, async } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { AppComponent } from './app.component'
import { UsecasesComponent } from '../../about/usecases/usecases.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { UsecaseComponent } from 'src/app/about/usecases/usecase/usecase.component'
import { DashboardComponent } from '../dashboard/dashboard.component'
import { NavbarComponent } from '../navbar/navbar.component'
import { BehaviorSubject } from 'rxjs'
import { AuthenticationService } from 'src/app/account/authentication.service'
import { AlertComponent } from 'src/app/alert/alert.component'
import { Component } from '@angular/core'

@Component({ selector: 'app-dashboard', template: '' })
class DashboardStubComponent {}

@Component({ selector: 'app-navbar', template: '' })
class NavbarStubComponent {}

@Component({ selector: 'app-usecase', template: '' })
class UsecaseStubComponent {}

@Component({ selector: 'app-about-usecases', template: '' })
class UsecasesStubComponent {}

@Component({ selector: 'app-alert', template: '' })
class AlertStubComponent {}

@Component({ selector: 'app-component', template: '' })
class ComponentStubComponent {}

describe('AppComponent', () => {
    let authServiceSpy: { userIsLoggedIn: jasmine.Spy }
    let alertServiceSpy

    beforeEach(async(() => {
        authServiceSpy = jasmine.createSpyObj('authenticationService', ['login', 'userIsLoggedIn'])
        authServiceSpy.userIsLoggedIn.and.returnValue(BehaviorSubject)
        alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])

        TestBed.configureTestingModule({
            imports: [RouterTestingModule, NgbModule],
            declarations: [
                AppComponent,
                ComponentStubComponent,
                DashboardStubComponent,
                NavbarStubComponent,
                UsecaseStubComponent,
                UsecasesStubComponent,
                AlertStubComponent
            ],
            providers: [{ provide: AuthenticationService, useValue: authServiceSpy }]
        }).compileComponents()
    }))

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent)
        const app = fixture.debugElement.componentInstance
        expect(app).toBeTruthy()
    })

    it(`should have as title 'Phone Review'`, () => {
        const fixture = TestBed.createComponent(AppComponent)
        const app = fixture.debugElement.componentInstance
        expect(app.title).toEqual('Phone Review')
    })
})
