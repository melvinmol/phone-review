import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { UsecaseComponent } from './about/usecases/usecase/usecase.component'
import { RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { HttpClientModule } from '@angular/common/http'
import { LoginComponent } from './account/login/login.component'
import { AlertService } from './alert/alert.service'
import { AuthenticationService } from './account/authentication.service'
import { ReactiveFormsModule } from '@angular/forms'
import { AddAccountComponent } from './account/add-account/add-account.component'
import { ComponentModule } from './component/component.module'
import { PhoneModule } from './phone/phone.module'
import { AlertComponent } from './alert/alert.component'
import { ComponentService } from './component/component.service'

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        UsecasesComponent,
        UsecaseComponent,
        DashboardComponent,
        LoginComponent,
        AddAccountComponent,
        AlertComponent
    ],
    imports: [
        ComponentModule,
        PhoneModule,
        BrowserModule,
        RouterModule,
        NgbModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
    providers: [AlertService, AuthenticationService, ComponentService],
    bootstrap: [AppComponent]
})
export class AppModule {}
