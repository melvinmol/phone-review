import { NgModule } from '@angular/core'
import { ComponentComponent } from './component.component'
import { AddComponentComponent } from './add-component/add-component.component'
import { ListComponentComponent } from './list-component/list-component.component'
import { ComponentRoutingModule } from './component-routing.module'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

@NgModule({
    declarations: [ComponentComponent, AddComponentComponent, ListComponentComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        // HttpModule,
        HttpClientModule,
        NgbModule,
        ComponentRoutingModule
    ],
    providers: [],
    exports: [ComponentComponent]
})
export class ComponentModule {}
