import { Component, OnInit } from '@angular/core'
import { Observable } from 'rxjs'
import { AuthenticationService } from '../account/authentication.service'

@Component({
    selector: 'app-component',
    templateUrl: './component.component.html',
    styleUrls: ['./component.component.scss']
})
export class ComponentComponent implements OnInit {
    isAdmin$: Observable<boolean>

    constructor(private authenticationService: AuthenticationService) {
        this.isAdmin$ = this.authenticationService.accountIsAdmin
    }

    ngOnInit() {}
}
