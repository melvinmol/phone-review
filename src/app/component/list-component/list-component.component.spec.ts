import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ListComponentComponent } from './list-component.component'
import { BehaviorSubject, observable, Observable, of } from 'rxjs'
import { AuthenticationService } from 'src/app/account/authentication.service'
import { AlertService } from 'src/app/alert/alert.service'
import { ComponentService } from '../component.service'
import { PhoneComponent } from 'src/modules/PhoneComponent'
import { Input, HostListener, Directive } from '@angular/core'

@Directive({
    // tslint:disable-next-line: directive-selector
    selector: '[routerLink]'
})
export class RouterLinkStubDirective {
    @Input('routerLink') linkParams: any
    navigatedTo: any = null

    @HostListener('click')
    onClick() {
        this.navigatedTo = this.linkParams
    }
}

describe('ListComponentComponent', () => {
    let component: ListComponentComponent
    let fixture: ComponentFixture<ListComponentComponent>
    let authServiceSpy
    let alertServiceSpy
    let componentServiceSpy: { getComponents: jasmine.Spy; deleteComponent: jasmine.Spy }

    beforeEach(async(() => {
        authServiceSpy = jasmine.createSpyObj('AuthenticationService', ['isAdmin$'])
        authServiceSpy.isAdmin$.and.returnValue(BehaviorSubject)

        alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])

        componentServiceSpy = jasmine.createSpyObj('ComponentService', ['getComponents', 'deleteComponent'])
        componentServiceSpy.getComponents.and.returnValue(of([]))

        TestBed.configureTestingModule({
            declarations: [ListComponentComponent, RouterLinkStubDirective],
            providers: [
                { provide: AuthenticationService, useValue: authServiceSpy },
                { provide: AlertService, useValue: alertServiceSpy },
                { provide: ComponentService, useValue: componentServiceSpy }
            ]
        }).compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(ListComponentComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('ngOnInit() should call getComponents()', () => {
        expect(componentServiceSpy.getComponents).toHaveBeenCalledTimes(1)
    })

    it('deleteComponent() called in componentService when calling', () => {
        component.deleteComponent('5de7c089b2803c696c8fd036')
        expect(componentServiceSpy.deleteComponent).toHaveBeenCalled()
    })
})
