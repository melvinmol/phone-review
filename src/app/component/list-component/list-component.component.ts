import { Component, OnInit, OnDestroy } from '@angular/core'
import { Observable, Subscription } from 'rxjs'
import { AuthenticationService } from 'src/app/account/authentication.service'
import { ComponentService } from '../component.service'
import { AlertService } from 'src/app/alert/alert.service'
import { PhoneComponent } from 'src/modules/PhoneComponent'

@Component({
    selector: 'app-list-component',
    templateUrl: './list-component.component.html',
    styleUrls: ['./list-component.component.scss']
})
export class ListComponentComponent implements OnInit, OnDestroy {
    isAdmin$: Observable<boolean>
    phoneComponents: PhoneComponent[]
    subs: Subscription

    constructor(
        private alertService: AlertService,
        private componentService: ComponentService,
        private authenticationService: AuthenticationService
    ) {
        this.isAdmin$ = this.authenticationService.accountIsAdmin
    }

    ngOnInit() {
        this.subs = this.componentService.getComponents().subscribe(
            phoneComponent => (this.phoneComponents = phoneComponent),
            error => {
                console.log(error)
                this.alertService.error(
                    '<strong>Connection error:</strong> ' + error + '<br/>(Is de server bereikbaar?)'
                )
            }
        )
    }

    ngOnDestroy() {
        if (this.subs) {
            this.subs.unsubscribe()
        }
    }

    deleteComponent(id: string) {
        this.componentService.deleteComponent(id)
        this.phoneComponents = this.phoneComponents.filter(phoneComponent => phoneComponent._id !== id)
    }
}
