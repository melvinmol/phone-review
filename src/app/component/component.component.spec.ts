import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ComponentComponent } from './component.component'
import { Observable, observable, BehaviorSubject, of } from 'rxjs'
import { AuthenticationService } from '../account/authentication.service'
import { ListComponentComponent } from './list-component/list-component.component'
import { Component } from '@angular/core'

describe('ComponentComponent', () => {
    @Component({ selector: 'app-list-component', template: '' })
    class ListComponentStubComponent {}

    @Component({ selector: 'router-outlet', template: '' })
    class RouterComponentStubComponent {}

    let component: ComponentComponent
    let fixture: ComponentFixture<ComponentComponent>

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ComponentComponent, ListComponentStubComponent, RouterComponentStubComponent],
            providers: [
                {
                    provide: AuthenticationService,
                    useValue: {
                        accountIsAdmin: of(true),
                        accountIsReviewer: of(true)
                    }
                }
            ]
        }).compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(ComponentComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('accountIsAdmin should be true', () => {
        expect(
            component.isAdmin$.subscribe(result => {
                expect(result).toEqual(true)
            })
        )
    })
})
