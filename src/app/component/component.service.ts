import { Injectable } from '@angular/core'
import { AlertService } from '../alert/alert.service'
import { Router } from '@angular/router'
import { environment } from 'src/environments/environment'
import { PhoneComponent } from 'src/modules/PhoneComponent'
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http'
import { tap, catchError, map } from 'rxjs/operators'
import { AuthenticationService } from '../account/authentication.service'
import { Observable, BehaviorSubject, throwError } from 'rxjs'

@Injectable({
    providedIn: 'root'
})
export class ComponentService {
    phoneComponents: PhoneComponent[] = []

    constructor(
        private alertService: AlertService,
        private http: HttpClient,
        private authenticationService: AuthenticationService
    ) {}

    public createComponent(type: string, description: string) {
        console.log('CreateComponent')
        console.log(`POST ${environment.apiUrl}/api/components`)
        const token = this.authenticationService.token
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: token
        })
        const phoneComponent = new PhoneComponent()
        phoneComponent.type = type
        phoneComponent.description = description
        this.phoneComponents.push(phoneComponent)
        return this.http
            .post(
                `${environment.apiUrl}/api/components`,
                {
                    type,
                    description
                },
                { headers }
            )
            .pipe(tap(data => console.log(data), error => console.error(error)))
            .subscribe({
                next: (response: any) => {
                    console.log(`Component id of created component ${response._id}`)
                    phoneComponent._id = response._id
                    this.alertService.success(`Component met het type '${type}' is aangemaakt`)
                },
                error: (message: any) => {
                    console.log('error:', message)
                    this.alertService.error('Er is iets mis gegaan bij het aanmaken van het component.')
                }
            })
    }

    public getComponents(): Observable<PhoneComponent[]> {
        console.log('getComponents')
        console.log(`Get ${environment.apiUrl}/api/components`)
        return this.http.get<ApiResponse>(`${environment.apiUrl}/api/components`).pipe(
            catchError(this.handleError),
            map(response => response.result.map(data => new PhoneComponent(data))),
            tap(phoneComponents => {
                this.phoneComponents = phoneComponents
            })
        )
    }

    public getComponent(id: string): PhoneComponent {
        console.log(`getComponent(${id})`)
        if (this.phoneComponents) {
            return this.phoneComponents.find(phoneComponent => phoneComponent._id === id)
        } else {
            return undefined
        }
    }

    public updateComponent(phoneComponent: PhoneComponent) {
        console.log('updatePhoneComponent')
        console.log(`${environment.apiUrl}/api/components/${phoneComponent._id}`)
        const token = this.authenticationService.token
        console.log(token)
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: token
        })
        const index = this.phoneComponents.findIndex(
            searchedPhoneComponent => searchedPhoneComponent._id === phoneComponent._id
        )
        this.phoneComponents[index] = phoneComponent
        return this.http
            .put(`${environment.apiUrl}/api/components/${phoneComponent._id}`, phoneComponent, { headers })
            .pipe(tap(data => console.log(data), error => console.error(error)))
            .subscribe({
                next: (response: any) => {
                    console.log(`Component id of created component ${response._id}`)
                    this.alertService.success(`Component met het type '${phoneComponent.type}' is gewijzigd`)
                },
                error: (message: any) => {
                    console.log('error:', message)
                    this.alertService.error('Er is iets mis gegaan bij het wijzigen van het component.')
                }
            })
    }

    public deleteComponent(id: string) {
        console.log('DeleteComponent')
        console.log(`Delete ${environment.apiUrl}/api/components/${id}`)
        const token = this.authenticationService.token
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: token
        })
        return this.http
            .delete(`${environment.apiUrl}/api/components/${id}`, { headers })
            .pipe(tap(data => console.log(data), error => console.error(error)))
            .subscribe({
                next: (response: any) => {
                    console.log(`Component id of created component ${response._id}`)
                    this.alertService.success(`Component is verwijderd.`)
                },
                error: (message: any) => {
                    console.log('error:', message)
                    this.alertService.error('Er is iets mis gegaan bij het verwijderen van het component.')
                }
            })
    }

    private handleError(error: HttpErrorResponse) {
        console.log('handleError')
        // return an observable with a component-facing error message
        return throwError(
            // 'Something bad happened; please try again later.'
            error.message || error.error.message
        )
    }
}

export interface ApiResponse {
    result: any[]
}
