import { Component, OnInit } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { ComponentService } from '../component.service'
import { Router, ActivatedRoute } from '@angular/router'
import { PhoneComponent } from 'src/modules/PhoneComponent'

@Component({
    selector: 'app-add-component',
    templateUrl: './add-component.component.html',
    styleUrls: ['./add-component.component.scss']
})
export class AddComponentComponent implements OnInit {
    componentForm: FormGroup
    title: string
    editMode: boolean
    phoneComponent: PhoneComponent

    constructor(
        private componentService: ComponentService,
        private route: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit() {
        console.log('TEST SNAP')
        console.log(this.route.snapshot.data)
        this.editMode = this.route.snapshot.data.userAlreadyExists || false
        this.title = this.route.snapshot.data.title || 'Component toevoegen'

        if (this.editMode) {
            this.route.params.subscribe(params => {
                const component = this.componentService.getComponent(params.id)
                // make local copy of user - detached from original array
                this.phoneComponent = new PhoneComponent(JSON.parse(JSON.stringify(component)))
            })
        } else {
            this.phoneComponent = new PhoneComponent()
        }
        this.componentForm = new FormGroup({
            type: new FormControl(null, [Validators.required, this.validType.bind(this)]),
            description: new FormControl(null, [Validators.required, this.validDescription.bind(this)])
        })
    }

    onSubmit() {
        if (this.componentForm.valid) {
            const type = this.componentForm.value['type']
            const description = this.componentForm.value['description']
            if (this.phoneComponent._id) {
                this.componentService.updateComponent(this.phoneComponent)
            } else {
                if (this.componentService.createComponent(type, description)) {
                    this.componentForm.reset()
                } else {
                    console.error('requist failed')
                }
            }
        } else {
            console.error('compoentForm invalid')
        }
    }

    validType(control: FormControl): { [s: string]: boolean } {
        const type = control.value
        const regexp = new RegExp('^.{1,}$')
        if (regexp.test(type) !== true) {
            return { type: false }
        } else {
            return null
        }
    }

    validDescription(control: FormControl): { [s: string]: boolean } {
        const description = control.value
        const regexp = new RegExp('^.{1,}$')
        if (regexp.test(description) !== true) {
            return { description: false }
        } else {
            return null
        }
    }
}
