import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { AddComponentComponent } from './add-component.component'
import { BehaviorSubject } from 'rxjs'
import { ReactiveFormsModule, FormControl, FormGroup } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { ComponentService } from '../component.service'

describe('AddComponentComponent', () => {
    let component: AddComponentComponent
    let fixture: ComponentFixture<AddComponentComponent>
    let componentServiceSpy: { createComponent: jasmine.Spy; updateComponent: jasmine.Spy }
    let routerSpy

    beforeEach(async(() => {
        componentServiceSpy = jasmine.createSpyObj('componentServiceSpy', [
            'createComponent',
            'updateComponent'
        ])
        componentServiceSpy.createComponent.and.returnValue(BehaviorSubject)
        componentServiceSpy.updateComponent.and.returnValue(BehaviorSubject)

        routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl'])

        TestBed.configureTestingModule({
            declarations: [AddComponentComponent],
            imports: [ReactiveFormsModule],
            providers: [
                { provide: Router, useValue: routerSpy },
                { provide: ComponentService, useValue: componentServiceSpy },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                userAlreadyExists: false,
                                title: 'Nice title'
                            }
                        }
                    }
                }
            ]
        }).compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(AddComponentComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('onSubmit() should create new component', () => {
        component.componentForm = new FormGroup({
            type: new FormControl('Name type'),
            description: new FormControl('Description')
        })
        component.onSubmit()
        expect(componentServiceSpy.createComponent).toHaveBeenCalled()
    })

    it('onSubmit() should not call createComponent, when phoneComponent as a _id', () => {
        component.componentForm = new FormGroup({
            type: new FormControl('Name type'),
            description: new FormControl('Description')
        })

        component.phoneComponent = {
            _id: '5de7c089b2803c696c8fd036',
            type: 'Name type',
            description: 'Description'
        }
        component.onSubmit()
        expect(componentServiceSpy.createComponent).toHaveBeenCalledTimes(0)
    })

    it('onSubmit() should update component', () => {
        component.componentForm = new FormGroup({
            type: new FormControl('Name type'),
            description: new FormControl('Description')
        })

        component.phoneComponent = {
            _id: '5de7c089b2803c696c8fd036',
            type: 'Name type',
            description: 'Description'
        }
        component.onSubmit()
        expect(componentServiceSpy.updateComponent).toHaveBeenCalled()
    })

    it('onSubmit() should not call updateComponent, when phoneComponent no _id', () => {
        component.componentForm = new FormGroup({
            type: new FormControl('Name type'),
            description: new FormControl('Description')
        })
        component.onSubmit()
        expect(componentServiceSpy.updateComponent).toHaveBeenCalledTimes(0)
    })

    it('validType() should be filled in', () => {
        const output = new FormControl('MelvinMol')
        expect(component.validType(output)).toEqual(null)
    })

    it('validType() should be not empty', () => {
        const output = new FormControl('')
        expect(component.validType(output)).toEqual({ type: false })
    })

    it('validDescription() should be filled in', () => {
        const output = new FormControl('MelvinMol')
        expect(component.validDescription(output)).toEqual(null)
    })

    it('validDescription() should be not empty', () => {
        const output = new FormControl('')
        expect(component.validDescription(output)).toEqual({ description: false })
    })
})
