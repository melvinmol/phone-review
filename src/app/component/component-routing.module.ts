import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { ComponentComponent } from './component.component'
import { ListComponentComponent } from './list-component/list-component.component'
import { AddComponentComponent } from './add-component/add-component.component'

const routes: Routes = [
    {
        path: 'component',
        component: ComponentComponent,
        children: [
            {
                path: 'add',
                component: AddComponentComponent,
                data: {
                    userAlreadyExists: false,
                    title: 'Component toevoegen'
                }
            },
            {
                path: ':id/edit',
                component: AddComponentComponent,
                data: {
                    userAlreadyExists: true,
                    title: 'Wijzig component'
                }
            }
        ]
    }
]

@NgModule({
    imports: [
        // Always use forChild in child route modules!
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class ComponentRoutingModule {}
