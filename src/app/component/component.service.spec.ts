import { TestBed } from '@angular/core/testing'

import { ComponentService } from './component.service'
import { BehaviorSubject, of } from 'rxjs'
import { PhoneComponent } from '../../modules/PhoneComponent'

describe('ComponentService', () => {
    let httpClientSpy: { get: jasmine.Spy; post: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy }
    let componentService: ComponentService
    let alertServiceSpy
    let authenticationServiceSpy

    beforeEach(() => {
        alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete'])
        httpClientSpy.get.and.returnValue(
            of(
                new PhoneComponent({
                    _id: '5de7c089b2803c696c8fd036',
                    type: 'component type',
                    description: 'component description'
                })
            )
        )
        httpClientSpy.post.and.returnValue(of({ _id: '5de7c089b2803c696c8fd036' }))
        httpClientSpy.put.and.returnValue(of({ _id: '5de7c089b2803c696c8fd036' }))
        httpClientSpy.delete.and.returnValue(of({ _id: '5de7c089b2803c696c8fd036' }))
        authenticationServiceSpy = jasmine.createSpyObj('authenticationService', ['isAdmin$'])
        authenticationServiceSpy.isAdmin$.and.returnValue(BehaviorSubject)

        componentService = new ComponentService(
            alertServiceSpy,
            httpClientSpy as any,
            authenticationServiceSpy
        )
        TestBed.configureTestingModule({})
    })

    it('should be created', () => {
        expect(componentService).toBeTruthy()
    })

    it('should show succes message when creating a component', () => {
        const component = new PhoneComponent({
            type: 'component type',
            description: 'component description'
        })
        const sub = componentService.createComponent(component.type, component.description)

        expect(alertServiceSpy.success.calls.count()).toBe(
            1,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.success).toHaveBeenCalled()
        expect(alertServiceSpy.error.calls.count()).toBe(
            0,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.error).not.toHaveBeenCalled()

        sub.unsubscribe()
    })

    it('should show succes message when updating a component', () => {
        const component = new PhoneComponent({
            type: 'component type',
            description: 'component description'
        })

        const newComponent = new PhoneComponent({
            type: 'new component type',
            description: 'new component description'
        })

        componentService.phoneComponents = []
        componentService.phoneComponents.push(component)

        const sub = componentService.updateComponent(newComponent)

        expect(alertServiceSpy.success.calls.count()).toBe(
            1,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.success).toHaveBeenCalled()
        expect(alertServiceSpy.error.calls.count()).toBe(
            0,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.error).not.toHaveBeenCalled()

        sub.unsubscribe()
    })

    it('should show succes message when deleting a component', () => {
        const component = new PhoneComponent({
            _id: '5de7c089b2803c696c8fd036',
            type: 'component type',
            description: 'component description'
        })

        componentService.phoneComponents = []
        componentService.phoneComponents.push(component)

        const sub = componentService.deleteComponent(component._id)

        expect(alertServiceSpy.success.calls.count()).toBe(
            1,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.success).toHaveBeenCalled()
        expect(alertServiceSpy.error.calls.count()).toBe(
            0,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.error).not.toHaveBeenCalled()

        sub.unsubscribe()
    })

    it('createPhone should add a phone to phones[]', () => {
        const component = new PhoneComponent({
            type: 'new component type',
            description: 'new component description'
        })
        const sub = componentService.createComponent(component.type, component.description)

        expect(componentService.phoneComponents.length).toBe(1, 'phoneComponents[] should have length 1')

        expect(componentService.phoneComponents[0].type).toEqual('new component type')
        expect(componentService.phoneComponents[0].description).toEqual('new component description')

        const component2 = new PhoneComponent({
            type: 'new component type2',
            description: 'new component description2'
        })
        componentService.createComponent(component2.type, component2.description)
        componentService.createComponent(component.type, component.description)
        componentService.createComponent(component2.type, component2.description)

        expect(componentService.phoneComponents[1].type).toEqual('new component type2')
        expect(componentService.phoneComponents[1].description).toEqual('new component description2')

        expect(componentService.phoneComponents[2].type).toEqual('new component type')
        expect(componentService.phoneComponents[2].description).toEqual('new component description')

        expect(componentService.phoneComponents[3].type).toEqual('new component type2')
        expect(componentService.phoneComponents[3].description).toEqual('new component description2')

        expect(componentService.phoneComponents.length).toBe(4, 'phones[] should have length 4')

        sub.unsubscribe()
    })
})
