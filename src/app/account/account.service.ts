import { Injectable, Type } from '@angular/core'
import { environment } from 'src/environments/environment'
import { tap } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { Account, AccountRole } from '../../modules/Account'
import { AlertService } from '../alert/alert.service'
import { AuthenticationService } from './authentication.service'

@Injectable({
    providedIn: 'root'
})
export class AccountService {
    constructor(
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private http: HttpClient
    ) {}

    createAccount(account: Account) {
        console.log('CreateAccount')
        console.log(`POST ${environment.apiUrl}/api/account`)
        const token = this.authenticationService.token
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: token
        })
        return this.http
            .post(
                `${environment.apiUrl}/api/account`,
                {
                    name: account.name,
                    email: account.email,
                    password: account.password,
                    role: AccountRole[account.role]
                },
                { headers }
            )
            .pipe(tap(data => console.log(data), error => console.error(error)))
            .subscribe({
                next: (response: any) => {
                    console.log(`Account id of created account ${response._id}`)
                    this.alertService.success(`Account met email '${account.email}' is aangemaakt`)
                    // If redirectUrl exists, go there
                    // this.router.navigate([this.redirectUrl]);
                },
                error: (message: any) => {
                    console.log('error:', message)
                    this.alertService.error('Er is iets mis gegaan bij het aanmaken van het account.')
                }
            })
    }
}
