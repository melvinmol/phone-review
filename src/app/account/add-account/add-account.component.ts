import { Component, OnInit, NgModule } from '@angular/core'
import { FormControl, Validators, FormGroup, NgModel } from '@angular/forms'
import { Subscription } from 'rxjs'
import { WebDriverLogger } from 'blocking-proxy/built/lib/webdriver_logger'
import { AccountService } from '../account.service'
import { Account } from 'src/modules/Account'
import { Router } from '@angular/router'

@Component({
    selector: 'app-add-account',
    templateUrl: './add-account.component.html',
    styleUrls: ['./add-account.component.scss']
})
@NgModule({
    providers: [AccountService]
})
export class AddAccountComponent implements OnInit {
    accountForm: FormGroup

    constructor(private accountService: AccountService, private router: Router) {}

    ngOnInit() {
        this.accountForm = new FormGroup({
            name: new FormControl(null, [Validators.required, this.validName.bind(this)]),
            email: new FormControl(null, [Validators.required, this.validEmail.bind(this)]),
            password: new FormControl(null, [Validators.required, this.validPassword.bind(this)]),
            role: new FormControl()
        })
    }

    onSubmit() {
        if (this.accountForm.valid) {
            const name = this.accountForm.value['name']
            const email = this.accountForm.value['email']
            const password = this.accountForm.value['password']
            const role = this.accountForm.value['role']
            if (
                this.accountService.createAccount(
                    new Account({
                        name,
                        email,
                        password,
                        role
                    })
                )
            ) {
                this.router.navigate(['/phone/dashboard'])
            } else {
                console.error('requist failed')
            }
        } else {
            console.error('loginForm invalid')
        }
    }

    validName(control: FormControl): { [s: string]: boolean } {
        const name = control.value
        const regexp = new RegExp('^[a-zA-Z]+$')
        if (regexp.test(name) !== true) {
            return { name: false }
        } else {
            return null
        }
    }

    validEmail(control: FormControl): { [s: string]: boolean } {
        const email = control.value
        const regexp = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        if (regexp.test(email) !== true) {
            return { email: false }
        } else {
            return null
        }
    }

    validPassword(control: FormControl): { [s: string]: boolean } {
        const password = control.value
        const regexp = new RegExp('^[a-zA-Z]([a-zA-Z0-9]){2,14}')
        if (regexp.test(password) !== true) {
            return { password: false }
        } else {
            return null
        }
    }
}
