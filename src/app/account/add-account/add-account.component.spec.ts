import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { AddAccountComponent } from './add-account.component'
import { ReactiveFormsModule, FormControl, FormGroup } from '@angular/forms'
import { Router } from '@angular/router'
import { BehaviorSubject } from 'rxjs'
import { AccountService } from '../account.service'

describe('AddAccountComponent', () => {
    let component: AddAccountComponent
    let fixture: ComponentFixture<AddAccountComponent>
    let accountServiceSpy: { createAccount: jasmine.Spy }
    let routerSpy: { navigate: jasmine.Spy }

    beforeEach(async(() => {
        accountServiceSpy = jasmine.createSpyObj('accountService', ['createAccount'])
        accountServiceSpy.createAccount.and.returnValue(BehaviorSubject)

        routerSpy = jasmine.createSpyObj('Router', ['navigate'])

        TestBed.configureTestingModule({
            declarations: [AddAccountComponent],
            imports: [ReactiveFormsModule],
            providers: [
                { provide: Router, useValue: routerSpy },
                { provide: AccountService, useValue: accountServiceSpy }
            ]
        }).compileComponents()

        fixture = TestBed.createComponent(AddAccountComponent)
        component = fixture.componentInstance
    }))

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('onSubmit() should create new account', () => {
        component.accountForm = new FormGroup({
            name: new FormControl('Name'),
            email: new FormControl('tester@tester.nl'),
            password: new FormControl('Sercet!!@@23'),
            role: new FormControl(3)
        })
        component.onSubmit()
        expect(accountServiceSpy.createAccount).toHaveBeenCalled()
    })

    it('validName() should accept only normal uppercase and lowercase letters', () => {
        const output = new FormControl('MelvinMol')
        expect(component.validName(output)).toEqual(null)
    })

    it('validName() should not accept numbers', () => {
        const output = new FormControl('123WWm')
        expect(component.validName(output)).toEqual({ name: false })
    })

    it('validName() should not accept spacial characters', () => {
        const output = new FormControl('#$%#@')
        expect(component.validName(output)).toEqual({ name: false })
    })

    it('validEmail() should accept valid email', () => {
        const output = new FormControl('tester@tester.nl')
        expect(component.validEmail(output)).toEqual(null)
    })

    it('validEmail() should not accept email without @', () => {
        const output = new FormControl('testertester.nl')
        expect(component.validEmail(output)).toEqual({ email: false })
    })

    it('validEmail() should not accept when field is empty', () => {
        const output = new FormControl('')
        expect(component.validEmail(output)).toEqual({ email: false })
    })

    it('validPassword() should be a strong password', () => {
        const output = new FormControl('G1eeTT34**')
        expect(component.validPassword(output)).toEqual(null)
    })

    it('validPassword() should not accept password with one char', () => {
        const output = new FormControl('c')
        expect(component.validPassword(output)).toEqual({ password: false })
    })
})
