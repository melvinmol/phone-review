import { TestBed, ComponentFixture, async } from '@angular/core/testing'

import { AuthenticationService } from './authentication.service'
import { HttpClient } from '@angular/common/http'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { throwError, of } from 'rxjs'

describe('AccountService', () => {
    let httpClientSpy: { get: jasmine.Spy; post: jasmine.Spy }
    let authService: AuthenticationService
    let alertServiceSpy
    let authServiceSpy

    const expectedSuccessResponse = {
        name: 'Firstname Lastname',
        email: 'user@host.com',
        role: 2,
        token:
            // tslint:disable-next-line: max-line-length
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NzQ2MDE4NjMsImlhdCI6MTU3MzczNzg2Mywic3ViIjp7ImVtYWlsIjoiYWRtaW5AYXZhbnMubmwiLCJpZCI6IjVkYzlhY2Y3NmUzOTVhMTY1ODkwMjk2MiJ9fQ.lyYNLeNEMHgdEEYpXaIQfDVKntDfTnR8IehJkDKNGFs'
    }
    beforeEach(() => {
        alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
        // alertServiceSpy.error.and.returnValue(stubValue)

        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post'])

        authServiceSpy = jasmine.createSpyObj('AuthService', ['saveCurrentUser'])

        const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl'])

        authService = new AuthenticationService(alertServiceSpy, routerSpy, httpClientSpy as any)
        authService.isLoggedInAccount.next(false)
        authService.loggedInAccountName.next('')
    })

    it('should be created', () => {
        expect(authService).toBeTruthy()
    })

    it('should login a user on a call to login() with valid user information', async(() => {
        // Set input and expected output
        const email = 'test@dummyserver.com'
        const password = 'secret'
        const expectedAlertMessage = 'You have been logged in'

        // Mock functions that are called on the way
        httpClientSpy.post.and.returnValue(of(expectedSuccessResponse))
        authServiceSpy.saveCurrentUser.and.returnValue()

        const subs = authService.login(email, password)

        expect(alertServiceSpy.success.calls.count()).toBe(
            1,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.success).toHaveBeenCalled()
        expect(alertServiceSpy.error.calls.count()).toBe(
            0,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.error).not.toHaveBeenCalled()

        authService.accountIsLoggedIn.subscribe(result => expect(result).toBe(true))

        // Clean up subscription
        subs.unsubscribe()
    }))

    it('should NOT login a user on a call to login() with INvalid user information', async(() => {
        // Set input and expected output
        const email = 'test@dummyserver.com'
        const password = 'secret'
        const expectedAlertMessage = 'Invalid credentials'
        const expectedErrorResponse = {
            error: { message: 'user not found' },
            name: 'HttpErrorResponse',
            ok: false,
            status: 401,
            statusText: 'Unauthorized'
        }

        // Mock functions that are called on the way
        // Make the http request fail; that is, return an Unauthorised message.
        httpClientSpy.post.and.returnValue(throwError(expectedErrorResponse))
        authServiceSpy.saveCurrentUser.and.returnValue()

        const subs = authService.login(email, password)

        expect(alertServiceSpy.error).toHaveBeenCalled()
        expect(alertServiceSpy.error.calls.count()).toBe(
            1,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.success).not.toHaveBeenCalled()
        expect(alertServiceSpy.success.calls.count()).toBe(
            0,
            'alertServiceSpy method must have been called once'
        )

        authService.accountIsLoggedIn.subscribe(result => expect(result).toBe(false))
        authService.accountFullName.subscribe(result => expect(result).toEqual(''))

        // Clean up subscription
        subs.unsubscribe()
    }))
})
