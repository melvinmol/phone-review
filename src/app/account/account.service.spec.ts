import { TestBed } from '@angular/core/testing'
import { AccountService } from './account.service'

describe('AddAccountService', () => {
    let httpClientSpy: { get: jasmine.Spy; post: jasmine.Spy }
    let accountService: AccountService
    let alertServiceSpy
    let authServiceSpy

    beforeEach(() => {
        alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post'])
        authServiceSpy = jasmine.createSpyObj('AuthService', ['saveCurrentUser'])

        accountService = new AccountService(authServiceSpy, authServiceSpy, httpClientSpy as any)
        TestBed.configureTestingModule({})
    })

    it('should be created', () => {
        expect(accountService).toBeTruthy()
    })
})
