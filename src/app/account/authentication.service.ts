import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { Account, AccountRole } from '../../modules/Account'
import { Router } from '@angular/router'
import { environment } from '../../environments/environment'
import { map, tap, catchError } from 'rxjs/operators'
import { AlertService } from '../alert/alert.service'
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    public isLoggedInAccount = new BehaviorSubject<boolean>(false)
    public loggedInAccountName = new BehaviorSubject<string>('')
    private isAdminAccount = new BehaviorSubject<boolean>(false)
    private isReviewerAccount = new BehaviorSubject<boolean>(false)
    private isPlainAccount = new BehaviorSubject<boolean>(false)

    private readonly currentAccount = 'currentaccount'
    private currentToken = 'token'

    // store the URL so we can redirect after logging in
    public readonly redirectUrl: string = '/dashboard'

    private readonly headers = new HttpHeaders({ 'Content-Type': 'application/json' })

    /**
     *
     * @param alertService Gives popup with status information
     * @param router Router of the application
     * @param http HttpClient
     */
    constructor(private alertService: AlertService, private router: Router, private http: HttpClient) {
        this.getCurrentAccount().subscribe({
            next: (account: Account) => {
                console.log(`${account.email} logged in`)
                this.isLoggedInAccount.next(true)
                this.loggedInAccountName.next(account.name)
                // Verify the possible roles the account can have.
                // Add more verifications when nessecary.
                console.log(account)
                account.hasRole(AccountRole.Admin).subscribe(result => this.isAdminAccount.next(result))
                account.hasRole(AccountRole.Reviewer).subscribe(result => this.isReviewerAccount.next(result))
                account.hasRole(AccountRole.Basic).subscribe(result => this.isPlainAccount.next(result))
            },
            error: message => {
                this.isLoggedInAccount.next(false)
                this.isAdminAccount.next(false)
                this.isReviewerAccount.next(false)
                this.isPlainAccount.next(false)
            }
        })
    }

    /**
     * Log in
     *
     * @param email Login email of the user
     * @param password Password of the user
     */
    login(email: string, password: string) {
        console.log('login')
        console.log(`POST ${environment.apiUrl}/api/login`)

        return this.http
            .post(`${environment.apiUrl}/api/login`, { email, password }, { headers: this.headers })
            .pipe(tap(data => console.log(data), error => console.error(error)))
            .subscribe({
                next: (response: any) => {
                    const currentAccount = new Account(response.account)
                    console.dir(currentAccount)
                    this.saveCurrentAccount(currentAccount, response.token)
                    // Notify all listeners that we're logged in.
                    this.isLoggedInAccount.next(true)
                    this.loggedInAccountName.next(currentAccount.name)
                    currentAccount
                        .hasRole(AccountRole.Admin)
                        .subscribe(result => this.isAdminAccount.next(result))
                    currentAccount
                        .hasRole(AccountRole.Basic)
                        .subscribe(result => this.isPlainAccount.next(result))
                    this.alertService.success('U bent ingelogd.')
                    // If redirectUrl exists, go there
                    // this.router.navigate([this.redirectUrl]);
                },
                error: (message: any) => {
                    console.log('error:', message)
                    this.alertService.error('Email of wachtwoord is onjuist')
                }
            })
    }

    /**
     * Log out.
     */
    logout() {
        console.log('logout')
        localStorage.removeItem(this.currentAccount)
        localStorage.removeItem(this.currentToken)
        this.isLoggedInAccount.next(false)
        this.isAdminAccount.next(false)
        this.alertService.success('You have been logged out.')
    }

    /**
     * Get the currently logged in account.
     */
    private getCurrentAccount(): Observable<Account> {
        return new Observable(observer => {
            const localAccount: any = JSON.parse(localStorage.getItem(this.currentAccount))
            if (localAccount) {
                console.log('localAccount found')
                observer.next(new Account(localAccount))
                observer.complete()
            } else {
                console.log('NO localAccount found')
                observer.error('NO localAccount found')
                observer.complete()
            }
        })
    }

    /**
     *
     */
    private saveCurrentAccount(account: Account, token: string): void {
        this.currentToken = token
        localStorage.setItem(this.currentAccount, JSON.stringify(account))
        localStorage.setItem(this.currentToken, JSON.stringify(token))
    }

    get accountFullName(): Observable<string> {
        console.log('accountFullName ' + this.loggedInAccountName.value)
        return this.loggedInAccountName.asObservable()
    }

    /**
     *
     */
    get accountIsLoggedIn(): Observable<boolean> {
        console.log('accountIsLoggedIn() ' + this.isLoggedInAccount.value)
        return this.isLoggedInAccount.asObservable()
    }

    /**
     *
     */
    get accountIsAdmin(): Observable<boolean> {
        console.log('accountIsAdmin() ' + this.isAdminAccount.value)
        return this.isAdminAccount.asObservable()
    }

    get accountIsReviewer(): Observable<boolean> {
        console.log('accountIsReviewer() ' + this.isReviewerAccount.value)
        return this.isReviewerAccount.asObservable()
    }

    /**
     *
     */
    get accountIsPlain(): Observable<boolean> {
        console.log('accountIsPlain() ' + this.isPlainAccount.value)
        return this.isPlainAccount.asObservable()
    }

    /**
     *
     */
    get token(): string {
        console.log('token() ' + this.currentToken)
        const localToken: any = JSON.parse(localStorage.getItem(this.currentToken))
        return localToken
    }
}
