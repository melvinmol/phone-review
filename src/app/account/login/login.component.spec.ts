import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { Router } from '@angular/router'
import { AuthenticationService } from '../authentication.service'

import { LoginComponent } from './login.component'
import { ReactiveFormsModule, FormGroup, FormControl } from '@angular/forms'
import { of, Observable, BehaviorSubject } from 'rxjs'

describe('LoginComponent', () => {
    let component: LoginComponent
    let fixture: ComponentFixture<LoginComponent>
    let routerSpy: { navigateByUrl: jasmine.Spy }
    let authServiceSpy: { userIsLoggedIn: jasmine.Spy; login: jasmine.Spy }

    beforeEach(async(() => {
        routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl'])
        authServiceSpy = jasmine.createSpyObj('authenticationService', ['login', 'userIsLoggedIn'])
        authServiceSpy.userIsLoggedIn.and.returnValue(BehaviorSubject)

        TestBed.configureTestingModule({
            declarations: [LoginComponent],
            imports: [ReactiveFormsModule],
            providers: [
                { provide: Router, useValue: routerSpy },
                { provide: AuthenticationService, useValue: authServiceSpy }
            ]
        }).compileComponents()

        fixture = TestBed.createComponent(LoginComponent)
        component = fixture.componentInstance
    }))

    afterEach(() => {
        fixture.destroy()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('should call login in AuthenticationService when submitting', () => {
        component.loginForm = new FormGroup({
            email: new FormControl('tester@tester.nl'),
            password: new FormControl('Sercet!!@@23')
        })
        component.onSubmit()
        expect(authServiceSpy.login).toHaveBeenCalled()
    })

    it('validEmail() should accept valid email', () => {
        const output = new FormControl('tester@tester.nl')
        expect(component.validEmail(output)).toEqual(null)
    })

    it('validEmail() should not accept email without @', () => {
        const output = new FormControl('testertester.nl')
        expect(component.validEmail(output)).toEqual({ email: false })
    })

    it('validPassword() should be a strong password', () => {
        const output = new FormControl('G1eeTT34**')
        expect(component.validPassword(output)).toEqual(null)
    })

    it('validPassword() should not accept password with one char', () => {
        const output = new FormControl('c')
        expect(component.validPassword(output)).toEqual({ password: false })
    })
})
