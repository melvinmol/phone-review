import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { DetailPhoneComponent } from './detail-phone.component'
import { PhoneService } from '../phone.service'
import { ActivatedRoute } from '@angular/router'
import { Phone } from 'src/modules/Phone'
import { Component } from '@angular/core'
import { of } from 'rxjs'

@Component({ selector: 'app-list-component-detail-phone', template: '' })
class ListComponentDetailPhoneStubComponent {}

@Component({ selector: 'app-review', template: '' })
class ReviewStubComponent {}

describe('DetailPhoneComponent', () => {
    let component: DetailPhoneComponent
    let fixture: ComponentFixture<DetailPhoneComponent>
    let phoneServiceSpy: { getPhone: jasmine.Spy }

    beforeEach(async(() => {
        const phone = new Phone({
            _id: '4thebgrfwdws',
            name: 'Name',
            company: 'Company',
            price: 12,
            components: [],
            reviews: []
        })

        phoneServiceSpy = jasmine.createSpyObj('phoneService', ['getPhone'])
        phoneServiceSpy.getPhone.and.returnValue(phone)

        TestBed.configureTestingModule({
            declarations: [DetailPhoneComponent, ListComponentDetailPhoneStubComponent, ReviewStubComponent],
            providers: [
                { provide: PhoneService, useValue: phoneServiceSpy },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        params: of({ id: 123 })
                    }
                }
            ]
        }).compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(DetailPhoneComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('ngOnInit() should call getPhone()', () => {
        expect(phoneServiceSpy.getPhone).toHaveBeenCalledTimes(1)
    })
})
