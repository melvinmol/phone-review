import { Component, OnInit } from '@angular/core'
import { PhoneService } from '../phone.service'
import { AlertService } from 'src/app/alert/alert.service'
import { Phone } from 'src/modules/Phone'
import { Subscription } from 'rxjs'
import { ActivatedRoute } from '@angular/router'

@Component({
    selector: 'app-detail-phone',
    templateUrl: './detail-phone.component.html',
    styleUrls: ['./detail-phone.component.scss']
})
export class DetailPhoneComponent implements OnInit {
    phone: Phone
    subs: Subscription

    constructor(private phoneService: PhoneService, private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.params.subscribe(params => {
            const phone = this.phoneService.getPhone(params.id)
            // make local copy of user - detached from original array
            this.phone = new Phone(JSON.parse(JSON.stringify(phone)))
        })
    }
}
