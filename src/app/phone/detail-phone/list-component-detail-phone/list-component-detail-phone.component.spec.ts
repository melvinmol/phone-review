import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ListComponentDetailPhoneComponent } from './list-component-detail-phone.component'
import { BehaviorSubject, of } from 'rxjs'
import { AuthenticationService } from 'src/app/account/authentication.service'
import { AlertService } from 'src/app/alert/alert.service'
import { PhoneService } from '../../phone.service'
import { Phone } from 'src/modules/Phone'
import { ActivatedRoute } from '@angular/router'

describe('ListComponentDetailPhoneComponent', () => {
    let component: ListComponentDetailPhoneComponent
    let fixture: ComponentFixture<ListComponentDetailPhoneComponent>
    let phoneServiceSpy: { getPhone: jasmine.Spy }

    beforeEach(async(() => {
        const phone = new Phone({
            _id: '4thebgrfwdws',
            name: 'Name',
            company: 'Company',
            price: 12,
            components: [],
            reviews: []
        })

        phoneServiceSpy = jasmine.createSpyObj('PhoneService', ['getPhone'])
        phoneServiceSpy.getPhone.and.returnValue(phone)

        TestBed.configureTestingModule({
            declarations: [ListComponentDetailPhoneComponent],
            providers: [
                { provide: PhoneService, useValue: phoneServiceSpy },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        params: of({ id: 123 })
                    }
                }
            ]
        }).compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(ListComponentDetailPhoneComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('ngOnInit() should call getPhone()', () => {
        expect(phoneServiceSpy.getPhone).toHaveBeenCalledTimes(1)
    })
})
