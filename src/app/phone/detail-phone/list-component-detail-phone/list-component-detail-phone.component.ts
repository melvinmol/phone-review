import { Component, OnInit } from '@angular/core'
import { Phone } from 'src/modules/Phone'
import { Subscription } from 'rxjs'
import { PhoneService } from '../../phone.service'
import { AlertService } from 'src/app/alert/alert.service'
import { ActivatedRoute } from '@angular/router'

@Component({
    selector: 'app-list-component-detail-phone',
    templateUrl: './list-component-detail-phone.component.html',
    styleUrls: ['./list-component-detail-phone.component.scss']
})
export class ListComponentDetailPhoneComponent implements OnInit {
    phone: Phone

    constructor(private phoneService: PhoneService, private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.params.subscribe(params => {
            const phone = this.phoneService.getPhone(params.id)
            console.log(params.id)
            // make local copy of user - detached from original array
            this.phone = new Phone(JSON.parse(JSON.stringify(phone)))
            console.log(phone)
        })
    }
}
