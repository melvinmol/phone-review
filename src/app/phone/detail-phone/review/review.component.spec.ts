import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ReviewComponent } from './review.component'
import { BehaviorSubject, of } from 'rxjs'
import { AuthenticationService } from 'src/app/account/authentication.service'
import { Component } from '@angular/core'

@Component({ selector: 'app-add-review', template: '' })
class AddReviewComponentStubComponent {}

@Component({ selector: 'app-list-review', template: '' })
class ListReviewComponentStubComponent {}

describe('ReviewComponent', () => {
    let component: ReviewComponent
    let fixture: ComponentFixture<ReviewComponent>

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ReviewComponent,
                AddReviewComponentStubComponent,
                ListReviewComponentStubComponent
            ],
            providers: [
                {
                    provide: AuthenticationService,
                    useValue: {
                        accountIsAdmin: of(true),
                        accountIsReviewer: of(true)
                    }
                }
            ]
        }).compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(ReviewComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('accountIsAdmin should be true', () => {
        expect(
            component.isAdmin$.subscribe(result => {
                expect(result).toEqual(true)
            })
        )
    })

    it('accountIsReviewer should be true', () => {
        expect(
            component.isReviewer$.subscribe(result => {
                expect(result).toEqual(true)
            })
        )
    })
})
