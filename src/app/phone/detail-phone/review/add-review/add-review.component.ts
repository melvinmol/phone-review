import { Component, OnInit, Input, Directive } from '@angular/core'
import { FormGroup, Validators, FormControl } from '@angular/forms'
import { Review, ConsOrPro } from 'src/modules/Review'
import { Subscription } from 'rxjs'
import { ReviewService } from '../review.service'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
    selector: 'app-add-review',
    templateUrl: './add-review.component.html',
    styleUrls: ['./add-review.component.scss']
})
export class AddReviewComponent implements OnInit {
    reviewForm: FormGroup
    review: Review
    descriptionConOrPro = ''
    conOrPro = false
    consOrPros: ConsOrPro[] = []
    subs: Subscription
    @Input() consOrProsDescription: string

    constructor(private reviewService: ReviewService, private route: ActivatedRoute) {}

    ngOnInit() {
        this.review = new Review()
        this.reviewForm = new FormGroup({
            title: new FormControl(null, [Validators.required, this.validTitle.bind(this)]),
            rating: new FormControl(null),
            description: new FormControl(null, [Validators.required, this.validDescription.bind(this)])
        })
    }

    onSubmit() {
        if (this.reviewForm.valid) {
            const title = this.reviewForm.value['title']
            const description = this.reviewForm.value['description']
            const rating = this.reviewForm.value['rating']
            console.log(rating)
            const newReview = new Review()
            newReview.title = title
            newReview.description = description
            switch (rating) {
                case 'oneStar':
                    newReview.rating = 1
                    break
                case 'twoStar':
                    newReview.rating = 2
                    break
                case 'threeStar':
                    newReview.rating = 3
                    break
                case 'fourStar':
                    newReview.rating = 4
                    break
                case 'fiveStar':
                    newReview.rating = 5
                    break
                default:
                // code block
            }
            newReview.consOrPros = this.consOrPros
            this.route.params.subscribe(params => {
                if (this.reviewService.createReview(newReview, params.id)) {
                    this.reviewForm.reset()
                    this.consOrPros = []
                } else {
                    console.error('requist failed')
                }
            })
        }
    }

    liveTyping(value: string) {
        this.consOrProsDescription = value
    }

    changeConOrPro(conOrPro) {
        if (conOrPro) {
            this.conOrPro = false
        } else {
            this.conOrPro = true
        }
    }

    addConOrProToReview() {
        console.log(this.conOrPro)
        console.log(this.consOrProsDescription)
        const newProOrCon = new ConsOrPro()
        newProOrCon.conOrPro = this.conOrPro
        newProOrCon.description = this.consOrProsDescription
        this.consOrPros.push(newProOrCon)
        this.conOrPro = true
    }

    validTitle(control: FormControl): { [s: string]: boolean } {
        const title = control.value
        const regexp = new RegExp('^.{1,}$')
        if (regexp.test(title) !== true) {
            return { title: false }
        } else {
            return null
        }
    }

    validDescription(control: FormControl): { [s: string]: boolean } {
        const description = control.value
        const regexp = new RegExp('^.{1,}$')
        if (regexp.test(description) !== true) {
            return { description: false }
        } else {
            return null
        }
    }

    validRating(control: FormControl): { [s: string]: boolean } {
        const rating = control.value
        const regexp = new RegExp('^([1-5])$')
        if (regexp.test(rating) !== true) {
            return { rating: false }
        } else {
            return null
        }
    }
}
