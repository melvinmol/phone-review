import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { AddReviewComponent } from './add-review.component'
import { PhoneService } from 'src/app/phone/phone.service'
import { ActivatedRoute } from '@angular/router'
import { of } from 'rxjs'
import { Phone } from 'src/modules/Phone'
import { ReviewService } from '../review.service'
import { ReactiveFormsModule, FormControl, FormGroup } from '@angular/forms'

describe('AddReviewComponent', () => {
    let component: AddReviewComponent
    let fixture: ComponentFixture<AddReviewComponent>
    let reviewServiceSpy: { createReview: jasmine.Spy }

    beforeEach(async(() => {
        const phone = new Phone({
            _id: '4thebgrfwdws',
            name: 'Name',
            company: 'Company',
            price: 12,
            components: [],
            reviews: []
        })

        reviewServiceSpy = jasmine.createSpyObj('ReviewService', ['createReview'])
        reviewServiceSpy.createReview.and.returnValue(of([]))

        TestBed.configureTestingModule({
            declarations: [AddReviewComponent],
            imports: [ReactiveFormsModule],
            providers: [
                { provide: ReviewService, useValue: reviewServiceSpy },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        params: of({ id: 123 })
                    }
                }
            ]
        }).compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(AddReviewComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('onSubmit() should create new account', () => {
        component.reviewForm = new FormGroup({
            name: new FormControl('Name'),
            email: new FormControl('tester@tester.nl'),
            rating: new FormControl(3)
        })

        component.consOrPros = []

        component.onSubmit()
        expect(reviewServiceSpy.createReview).toHaveBeenCalled()
    })

    it('validDescription() should be filled in', () => {
        const output = new FormControl('MelvinMol')
        expect(component.validDescription(output)).toEqual(null)
    })

    it('validDescription() sshould return false when is empty', () => {
        const output = new FormControl('')
        expect(component.validDescription(output)).toEqual({ description: false })
    })

    it('validTitle() should be filled in', () => {
        const output = new FormControl('MelvinMol')
        expect(component.validTitle(output)).toEqual(null)
    })

    it('validTitle() should return false when is empty', () => {
        const output = new FormControl('')
        expect(component.validTitle(output)).toEqual({ title: false })
    })
})
