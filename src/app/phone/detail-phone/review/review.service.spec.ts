import { TestBed } from '@angular/core/testing'

import { ReviewService } from './review.service'
import { BehaviorSubject, of } from 'rxjs'

describe('ReviewService', () => {
    let httpClientSpy: { get: jasmine.Spy; post: jasmine.Spy }
    let reviewService: ReviewService
    let phoneServiceSpy
    let alertServiceSpy
    let authenticationServiceSpy

    beforeEach(() => {
        alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post'])
        httpClientSpy.post.and.returnValue(of({ _id: '5de7926b182d4f0017e560b4' }))
        phoneServiceSpy = jasmine.createSpyObj('PhoneService', ['phones'])
        phoneServiceSpy.phones.and.returnValue([])

        authenticationServiceSpy = jasmine.createSpyObj('authenticationService', ['isAdmin$'])
        authenticationServiceSpy.isAdmin$.and.returnValue(BehaviorSubject)

        reviewService = new ReviewService(
            authenticationServiceSpy,
            httpClientSpy as any,
            alertServiceSpy,
            phoneServiceSpy
        )
    })

    it('should be created', () => {
        expect(reviewService).toBeTruthy()
    })
})
