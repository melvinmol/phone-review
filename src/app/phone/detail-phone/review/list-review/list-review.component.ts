import { Component, OnInit } from '@angular/core'
import { Review } from 'src/modules/Review'
import { PhoneService } from 'src/app/phone/phone.service'
import { AlertService } from 'src/app/alert/alert.service'
import { ActivatedRoute } from '@angular/router'
import { Phone } from 'src/modules/Phone'

@Component({
    selector: 'app-list-review',
    templateUrl: './list-review.component.html',
    styleUrls: ['./list-review.component.scss']
})
export class ListReviewComponent implements OnInit {
    reviews: Review[]

    constructor(private phoneService: PhoneService, private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.params.subscribe(params => {
            const phone = this.phoneService.getPhone(params.id)
            console.log(params.id)
            // make local copy of user - detached from original array
            this.reviews = new Phone(JSON.parse(JSON.stringify(phone))).reviews
        })
    }

    createRange(number) {
        const items: number[] = []
        for (let i = 1; i <= number; i++) {
            items.push(i)
        }
        return items
    }
}
