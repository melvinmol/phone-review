import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ListReviewComponent } from './list-review.component'
import { Phone } from 'src/modules/Phone'
import { PhoneService } from 'src/app/phone/phone.service'
import { ActivatedRoute } from '@angular/router'
import { of } from 'rxjs'

describe('ListReviewComponent', () => {
    let component: ListReviewComponent
    let fixture: ComponentFixture<ListReviewComponent>
    let phoneServiceSpy: { getPhone: jasmine.Spy }

    beforeEach(() => {
        const phone = new Phone()
        phone._id = 'grvfcedxwsxfrgvfecdxs'
        phoneServiceSpy = jasmine.createSpyObj('phoneService', ['getPhone'])
        phoneServiceSpy.getPhone.and.returnValue(of([]))

        TestBed.configureTestingModule({
            declarations: [ListReviewComponent],
            providers: [
                { provide: PhoneService, useValue: phoneServiceSpy },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        params: of({ id: 123 })
                    }
                }
            ]
        }).compileComponents()

        fixture = TestBed.createComponent(ListReviewComponent)

        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('ngOnInit() should call getPhone()', () => {
        expect(phoneServiceSpy.getPhone).toHaveBeenCalledTimes(1)
    })
})
