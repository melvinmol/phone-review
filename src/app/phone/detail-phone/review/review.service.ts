import { Injectable } from '@angular/core'
import { AuthenticationService } from 'src/app/account/authentication.service'
import { environment } from 'src/environments/environment'
import { PhoneService } from '../../phone.service'
import { Review } from 'src/modules/Review'
import { HttpHeaders, HttpClient } from '@angular/common/http'
import { AlertService } from 'src/app/alert/alert.service'
import { tap } from 'rxjs/operators'

@Injectable({
    providedIn: 'root'
})
export class ReviewService {
    constructor(
        private authenticationService: AuthenticationService,
        private http: HttpClient,
        private alertService: AlertService,
        private phoneSevice: PhoneService
    ) {}

    public createReview(review: Review, phoneId: string) {
        const index = this.phoneSevice.phones.findIndex(searchedPhone => searchedPhone._id === phoneId)
        this.phoneSevice.phones[index].reviews.push(review)
        console.log('CreateReview')
        console.log(`POST ${environment.apiUrl}/api/phones/${phoneId}/reviews`)
        const token = this.authenticationService.token
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: token
        })
        return this.http
            .post(`${environment.apiUrl}/api/phones/${phoneId}/reviews`, review, { headers })
            .pipe(tap(data => console.log(data), error => console.error(error)))
            .subscribe({
                next: (response: any) => {
                    console.log(`Review id of created review ${response._id}`)
                    review._id = response._id
                    this.alertService.success(`Review is met de titel '${review.title}' aangemaakt`)
                },
                error: (message: any) => {
                    console.log('error:', message)
                    this.alertService.error('Er is iets mis gegaan bij het aanmaken van de review.')
                }
            })
    }
}
