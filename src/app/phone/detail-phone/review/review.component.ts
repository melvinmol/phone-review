import { Component, OnInit } from '@angular/core'
import { Observable } from 'rxjs'
import { AuthenticationService } from 'src/app/account/authentication.service'

@Component({
    selector: 'app-review',
    templateUrl: './review.component.html',
    styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
    isReviewer$: Observable<boolean>
    isAdmin$: Observable<boolean>

    constructor(private authenticationService: AuthenticationService) {
        this.isAdmin$ = this.authenticationService.accountIsAdmin
        this.isReviewer$ = this.authenticationService.accountIsReviewer
    }

    ngOnInit() {}
}
