import { NgModule } from '@angular/core'
import { PhoneComponent } from './phone.component'
import { AddPhoneComponent } from './add-phone/add-phone.component'
import { ListPhoneComponent } from './list-phone/list-phone.component'
import { PhoneRoutingModule } from './phone-routing.module'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { PhoneService } from './phone.service'
import { DashboardPhoneComponent } from './dashboard-phone/dashboard-phone.component'
import { DetailPhoneComponent } from './detail-phone/detail-phone.component'
import { ListComponentDetailPhoneComponent } from './detail-phone/list-component-detail-phone/list-component-detail-phone.component'
import { ReviewComponent } from './detail-phone/review/review.component'
import { AddReviewComponent } from './detail-phone/review/add-review/add-review.component'
import { ListReviewComponent } from './detail-phone/review/list-review/list-review.component'

@NgModule({
    declarations: [
        PhoneComponent,
        AddPhoneComponent,
        ListPhoneComponent,
        DashboardPhoneComponent,
        DetailPhoneComponent,
        ListComponentDetailPhoneComponent,
        ReviewComponent,
        AddReviewComponent,
        ListReviewComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        // HttpModule,
        HttpClientModule,
        NgbModule,
        PhoneRoutingModule
    ],
    providers: [PhoneService],
    exports: [PhoneComponent]
})
export class PhoneModule {}
