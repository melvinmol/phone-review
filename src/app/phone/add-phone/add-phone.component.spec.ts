import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { AddPhoneComponent } from './add-phone.component'
import { BehaviorSubject, of } from 'rxjs'
import { ReactiveFormsModule, FormControl } from '@angular/forms'
import { PhoneService } from '../phone.service'
import { ComponentService } from 'src/app/component/component.service'
import { ActivatedRoute } from '@angular/router'
import { AlertService } from 'src/app/alert/alert.service'

describe('AddPhoneComponent', () => {
    let component: AddPhoneComponent
    let fixture: ComponentFixture<AddPhoneComponent>
    let phoneServiceSpy: { createPhone: jasmine.Spy; updatePhone: jasmine.Spy; getPhone: jasmine.Spy }
    let componentServiceSpy: { getComponents: jasmine.Spy }
    let alertServiceSpy

    beforeEach(async(() => {
        phoneServiceSpy = jasmine.createSpyObj('PhoneService', ['createPhone', 'updatePhone', 'getPhone'])
        phoneServiceSpy.createPhone.and.returnValue(of([]))
        phoneServiceSpy.updatePhone.and.returnValue(of([]))
        phoneServiceSpy.getPhone.and.returnValue(of([]))

        componentServiceSpy = jasmine.createSpyObj('ComponentService', ['getComponents'])
        componentServiceSpy.getComponents.and.returnValue(of([]))

        alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])

        TestBed.configureTestingModule({
            declarations: [AddPhoneComponent],
            imports: [ReactiveFormsModule],
            providers: [
                { provide: PhoneService, useValue: phoneServiceSpy },
                { provide: ComponentService, useValue: componentServiceSpy },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                userAlreadyExists: false,
                                title: 'Nice title'
                            }
                        }
                    }
                },
                { provide: AlertService, useValue: alertServiceSpy }
            ]
        }).compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(AddPhoneComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('validName() should be accept filled form', () => {
        const output = new FormControl('MelvinMol')
        expect(component.validName(output)).toEqual(null)
    })

    it('validName() should return false when form is empty', () => {
        const output = new FormControl('')
        expect(component.validName(output)).toEqual({ name: false })
    })

    it('validCompany() should be accept filled form', () => {
        const output = new FormControl('company')
        expect(component.validCompany(output)).toEqual(null)
    })

    it('validCompany() should return false when form is empty', () => {
        const output = new FormControl('')
        expect(component.validCompany(output)).toEqual({ company: false })
    })

    it('validPrice() should accept valid number', () => {
        const output = new FormControl(600)
        expect(component.validPrice(output)).toEqual(null)
    })

    it('validPrice() should accept valid number with two didgets after the ,', () => {
        const output = new FormControl(600.22)
        expect(component.validPrice(output)).toEqual(null)
    })

    it('validPrice() should not accept number with three didgets after the ,', () => {
        const output = new FormControl(600.222)
        expect(component.validPrice(output)).toEqual({ price: false })
    })

    it('validPrice() should not accept negative number', () => {
        const output = new FormControl(-20)
        expect(component.validPrice(output)).toEqual({ price: false })
    })
})
