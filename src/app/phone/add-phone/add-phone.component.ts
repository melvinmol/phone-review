import { Component, OnInit } from '@angular/core'
import { FormGroup, Validators, FormControl } from '@angular/forms'
import { Phone } from 'src/modules/Phone'
import { PhoneService } from '../phone.service'
import { Router, ActivatedRoute } from '@angular/router'
import { PhoneComponent } from '../../../modules/PhoneComponent'
import { Subscription } from 'rxjs'
import { ComponentService, ApiResponse } from 'src/app/component/component.service'
import { AlertService } from 'src/app/alert/alert.service'

@Component({
    selector: 'app-add-phone',
    templateUrl: './add-phone.component.html',
    styleUrls: ['./add-phone.component.scss']
})
export class AddPhoneComponent implements OnInit {
    phoneForm: FormGroup
    title: string
    editMode: boolean
    phone: Phone
    phoneComponents: PhoneComponent[]
    componentsAddedToPhone: PhoneComponent[] = []
    subs: Subscription

    constructor(
        private phoneService: PhoneService,
        private route: ActivatedRoute,
        private componentService: ComponentService,
        private alertService: AlertService
    ) {}

    ngOnInit() {
        this.title = this.route.snapshot.data.title || 'Phone toevoegen'
        this.editMode = this.route.snapshot.data.phoneAlreadyExists || false

        console.log(this.editMode)
        if (this.editMode) {
            this.route.params.subscribe(params => {
                const phone = this.phoneService.getPhone(params.id)
                // make local copy of user - detached from original array
                this.phone = new Phone(JSON.parse(JSON.stringify(phone)))
                console.log(phone)
                this.componentsAddedToPhone = phone.components
            })
        } else {
            this.phone = new Phone()
        }
        this.phoneForm = new FormGroup({
            name: new FormControl(null, [Validators.required, this.validName.bind(this)]),
            company: new FormControl(null, [Validators.required, this.validCompany.bind(this)]),
            price: new FormControl(null, [Validators.required, this.validPrice.bind(this)])
        })

        this.subs = this.componentService.getComponents().subscribe(
            phoneComponent => (this.phoneComponents = phoneComponent),
            error => {
                console.log(error)
                this.alertService.error(
                    '<strong>Connection error:</strong> ' + error + '<br/>(Is de server bereikbaar?)'
                )
            }
        )
    }

    onSubmit() {
        if (this.phoneForm.valid) {
            const name = this.phoneForm.value['name']
            const company = this.phoneForm.value['company']
            const price = this.phoneForm.value['price']
            if (this.phone._id) {
                this.phone.name = name
                this.phone.company = company
                this.phone.price = price
                this.phone.components = this.componentsAddedToPhone
                this.phoneService.updatePhone(this.phone)
            } else {
                const newPhone = new Phone()
                newPhone.name = name
                newPhone.company = company
                newPhone.price = price
                newPhone.components = this.componentsAddedToPhone
                if (this.phoneService.createPhone(newPhone)) {
                    this.phoneForm.reset()
                    this.componentsAddedToPhone = []
                } else {
                    console.error('requist failed')
                }
            }
        } else {
            console.error('phoneForm invalid')
        }
    }

    addComponentToPhone(component: PhoneComponent) {
        console.log(component)
        this.componentsAddedToPhone.push(component)
    }

    validName(control: FormControl): { [s: string]: boolean } {
        const name = control.value
        const regexp = new RegExp('^.{1,}$')
        if (regexp.test(name) !== true) {
            return { name: false }
        } else {
            return null
        }
    }

    validCompany(control: FormControl): { [s: string]: boolean } {
        const company = control.value
        const regexp = new RegExp('^.{1,}$')
        if (regexp.test(company) !== true) {
            return { company: false }
        } else {
            return null
        }
    }

    validPrice(control: FormControl): { [s: string]: boolean } {
        const price = control.value
        const regexp = new RegExp('^[0-9]+(.[0-9]{1,2})?$')
        if (regexp.test(price) !== true) {
            return { price: false }
        } else {
            return null
        }
    }

    validComponent(control: FormControl): { [s: string]: boolean } {
        if (this.componentsAddedToPhone.length >= 0) {
            return { price: false }
        } else {
            return null
        }
    }
}
