import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { PhoneComponent } from './phone.component'

import { DashboardPhoneComponent } from './dashboard-phone/dashboard-phone.component'
import { AddPhoneComponent } from './add-phone/add-phone.component'
import { DetailPhoneComponent } from './detail-phone/detail-phone.component'

const routes: Routes = [
    {
        path: 'phone',
        component: PhoneComponent,
        children: [
            {
                path: 'dashboard',
                component: DashboardPhoneComponent,
                children: [
                    {
                        path: 'add',
                        component: AddPhoneComponent,
                        data: {
                            phoneAlreadyExists: false,
                            title: 'Telefoon toevoegen'
                        }
                    },
                    {
                        path: ':id/edit',
                        component: AddPhoneComponent,
                        data: {
                            phoneAlreadyExists: true,
                            title: 'Wijzig telefoon'
                        }
                    }
                ]
            },
            {
                path: ':id',
                component: DetailPhoneComponent
            }
        ]
    }
]

@NgModule({
    imports: [
        // Always use forChild in child route modules!
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class PhoneRoutingModule {}
