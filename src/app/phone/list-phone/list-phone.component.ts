import { Component, OnInit, OnDestroy } from '@angular/core'
import { Phone } from 'src/modules/Phone'
import { Observable, Subscription } from 'rxjs'
import { AlertService } from 'src/app/alert/alert.service'
import { PhoneService } from '../phone.service'
import { AuthenticationService } from 'src/app/account/authentication.service'

@Component({
    selector: 'app-list-phone',
    templateUrl: './list-phone.component.html',
    styleUrls: ['./list-phone.component.scss']
})
export class ListPhoneComponent implements OnInit, OnDestroy {
    isAdmin$: Observable<boolean>
    phones: Phone[]
    subs: Subscription

    constructor(
        private alertService: AlertService,
        private phoneService: PhoneService,
        private authenticationService: AuthenticationService
    ) {
        this.isAdmin$ = this.authenticationService.accountIsAdmin
    }

    ngOnInit() {
        this.subs = this.phoneService.getPhones().subscribe(
            phone => (this.phones = phone),
            error => {
                console.log(error)
                this.alertService.error(
                    '<strong>Connection error:</strong> ' + error + '<br/>(Is de server bereikbaar?)'
                )
            }
        )
    }

    ngOnDestroy() {
        if (this.subs) {
            this.subs.unsubscribe()
        }
    }

    deletePhone(id: string) {
        this.phoneService.deletePhone(id)
        this.phones = this.phones.filter(phone => phone._id !== id)
    }
}
