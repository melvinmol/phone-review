import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ListPhoneComponent } from './list-phone.component'
import { Component, Directive, Input, HostListener } from '@angular/core'
import { BehaviorSubject, of } from 'rxjs'
import { AuthenticationService } from 'src/app/account/authentication.service'
import { AlertService } from 'src/app/alert/alert.service'
import { PhoneService } from '../phone.service'

@Directive({
    // tslint:disable-next-line: directive-selector
    selector: '[routerLink]'
})
export class RouterLinkStubDirective {
    @Input('routerLink') linkParams: any
    navigatedTo: any = null

    @HostListener('click')
    onClick() {
        this.navigatedTo = this.linkParams
    }
}
describe('ListPhoneComponent', () => {
    let component: ListPhoneComponent
    let fixture: ComponentFixture<ListPhoneComponent>
    let alertServiceSpy
    let phoneServiceSpy: { getPhones: jasmine.Spy; deletePhone: jasmine.Spy }
    let authenticationServiceSpay

    beforeEach(async(() => {
        authenticationServiceSpay = jasmine.createSpyObj('authenticationService', ['isAdmin$'])
        authenticationServiceSpay.isAdmin$.and.returnValue(BehaviorSubject)

        alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
        phoneServiceSpy = jasmine.createSpyObj('PhoneService', ['getPhones', 'deletePhone'])
        phoneServiceSpy.getPhones.and.returnValue(of([]))

        TestBed.configureTestingModule({
            declarations: [ListPhoneComponent, RouterLinkStubDirective],
            providers: [
                { provide: AuthenticationService, useValue: authenticationServiceSpay },
                { provide: AlertService, useValue: alertServiceSpy },
                { provide: PhoneService, useValue: phoneServiceSpy }
            ]
        }).compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(ListPhoneComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('ngOnInit() should call getComponents()', () => {
        expect(phoneServiceSpy.getPhones).toHaveBeenCalledTimes(1)
    })

    it('deletePhone() called in componentService when calling', () => {
        component.deletePhone('5de7c089b2803c696c8fd036')
        expect(phoneServiceSpy.deletePhone).toHaveBeenCalled()
    })
})
