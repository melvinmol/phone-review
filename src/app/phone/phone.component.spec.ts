import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { PhoneComponent } from './phone.component'
import { Component } from '@angular/core'
import { BehaviorSubject } from 'rxjs'
import { AuthenticationService } from '../account/authentication.service'

describe('PhoneComponent', () => {
    @Component({ selector: 'app-list-phone', template: '' })
    class ListPhoneStubComponent {}

    @Component({ selector: 'router-outlet', template: '' })
    class RouterPhoneStubComponent {}

    let component: PhoneComponent
    let fixture: ComponentFixture<PhoneComponent>
    let authServiceSpy

    beforeEach(async(() => {
        authServiceSpy = jasmine.createSpyObj('authenticationService', ['isAdmin$'])
        authServiceSpy.isAdmin$.and.returnValue(BehaviorSubject)

        TestBed.configureTestingModule({
            declarations: [PhoneComponent, ListPhoneStubComponent, RouterPhoneStubComponent],
            providers: [{ provide: AuthenticationService, useValue: authServiceSpy }]
        }).compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(PhoneComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
