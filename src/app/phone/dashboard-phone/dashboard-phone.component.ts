import { Component, OnInit } from '@angular/core'
import { Observable } from 'rxjs'
import { AuthenticationService } from 'src/app/account/authentication.service'

@Component({
    selector: 'app-dashboard-phone',
    templateUrl: './dashboard-phone.component.html',
    styleUrls: ['./dashboard-phone.component.scss']
})
export class DashboardPhoneComponent implements OnInit {
    isAdmin$: Observable<boolean>

    constructor(private authenticationService: AuthenticationService) {
        this.isAdmin$ = this.authenticationService.accountIsAdmin
    }

    ngOnInit() {}
}
