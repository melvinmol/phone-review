import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { DashboardPhoneComponent } from './dashboard-phone.component'
import { Component } from '@angular/core'
import { BehaviorSubject, of } from 'rxjs'
import { AuthenticationService } from 'src/app/account/authentication.service'

@Component({ selector: 'app-list-phone', template: '' })
class ListPhoneStubComponent {}

@Component({ selector: 'router-outlet', template: '' })
class RouterPhoneStubComponent {}

describe('DashboardPhoneComponent', () => {
    let component: DashboardPhoneComponent
    let fixture: ComponentFixture<DashboardPhoneComponent>

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardPhoneComponent, ListPhoneStubComponent, RouterPhoneStubComponent],
            providers: [
                {
                    provide: AuthenticationService,
                    useValue: {
                        accountIsAdmin: of(true)
                    }
                }
            ]
        }).compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardPhoneComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })

    it('accountIsAdmin should be true', () => {
        expect(
            component.isAdmin$.subscribe(result => {
                expect(result).toEqual(true)
            })
        )
    })
})
