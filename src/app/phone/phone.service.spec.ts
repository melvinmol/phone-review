import { TestBed } from '@angular/core/testing'

import { PhoneService } from './phone.service'
import { BehaviorSubject, of } from 'rxjs'
import { Phone } from 'src/modules/Phone'

describe('PhoneService', () => {
    let httpClientSpy: { get: jasmine.Spy; post: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy }
    let phoneService: PhoneService
    let alertServiceSpy
    let authenticationServiceSpy

    beforeEach(() => {
        alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete'])
        httpClientSpy.get.and.returnValue(
            of(
                new Phone({
                    name: 'phone name',
                    company: 'company name',
                    price: 500,
                    components: []
                })
            )
        )
        httpClientSpy.post.and.returnValue(of({ _id: '5de7c089b2803c696c8fd036' }))
        httpClientSpy.put.and.returnValue(of({ _id: '5de7c089b2803c696c8fd036' }))
        httpClientSpy.delete.and.returnValue(of({ _id: '5de7c089b2803c696c8fd036' }))

        authenticationServiceSpy = jasmine.createSpyObj('authenticationService', ['isAdmin$'])
        authenticationServiceSpy.isAdmin$.and.returnValue(BehaviorSubject)

        phoneService = new PhoneService(alertServiceSpy, httpClientSpy as any, authenticationServiceSpy)
        TestBed.configureTestingModule({})
    })

    it('should be created', () => {
        expect(phoneService).toBeTruthy()
    })

    it('should show succes message when creating a phone', () => {
        const phone = new Phone({
            name: 'phone name',
            company: 'company name',
            price: 500,
            components: []
        })
        const sub = phoneService.createPhone(phone)

        expect(alertServiceSpy.success.calls.count()).toBe(
            1,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.success).toHaveBeenCalled()
        expect(alertServiceSpy.error.calls.count()).toBe(
            0,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.error).not.toHaveBeenCalled()

        sub.unsubscribe()
    })

    it('should show succes message when updating a phone', () => {
        const phone = new Phone({
            _id: '5de7c089b2803c696c8fd036',
            name: 'phone name',
            company: 'company name',
            price: 500,
            components: []
        })

        const updatedPhone = new Phone({
            _id: '5de7c089b2803c696c8fd036',
            name: 'new phone name',
            company: 'new company name',
            price: 700,
            components: []
        })

        phoneService.phones = []
        phoneService.phones.push(phone)

        const sub = phoneService.updatePhone(updatedPhone)

        expect(alertServiceSpy.success.calls.count()).toBe(
            1,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.success).toHaveBeenCalled()
        expect(alertServiceSpy.error.calls.count()).toBe(
            0,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.error).not.toHaveBeenCalled()

        sub.unsubscribe()
    })

    it('should show succes message when deleting a phone', () => {
        const phone = new Phone({
            _id: '5de7c089b2803c696c8fd036',
            name: 'phone name',
            company: 'company name',
            price: 500,
            components: []
        })

        phoneService.phones = []
        phoneService.phones.push(phone)

        const sub = phoneService.deletePhone(phone._id)

        expect(alertServiceSpy.success.calls.count()).toBe(
            1,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.success).toHaveBeenCalled()
        expect(alertServiceSpy.error.calls.count()).toBe(
            0,
            'alertServiceSpy method must have been called once'
        )
        expect(alertServiceSpy.error).not.toHaveBeenCalled()

        sub.unsubscribe()
    })

    it('createPhone should add a phone to phones[]', () => {
        const phone = new Phone({
            name: 'phone name',
            company: 'company name',
            price: 500,
            components: []
        })
        const sub = phoneService.createPhone(phone)

        expect(phoneService.phones.length).toBe(1, 'phones[] should have length 1')

        expect(phoneService.phones[0].name).toEqual('phone name')
        expect(phoneService.phones[0].company).toEqual('company name')
        expect(phoneService.phones[0].price).toEqual(500)
        expect(phoneService.phones[0].components).toEqual([])

        const phone2 = new Phone({
            name: 'phone name2',
            company: 'company name2',
            price: 600,
            components: []
        })
        phoneService.createPhone(phone2)
        phoneService.createPhone(phone)
        phoneService.createPhone(phone2)

        expect(phoneService.phones[1].name).toEqual('phone name2')
        expect(phoneService.phones[1].company).toEqual('company name2')
        expect(phoneService.phones[1].price).toEqual(600)
        expect(phoneService.phones[1].components).toEqual([])

        expect(phoneService.phones[2].name).toEqual('phone name')
        expect(phoneService.phones[2].company).toEqual('company name')
        expect(phoneService.phones[2].price).toEqual(500)
        expect(phoneService.phones[2].components).toEqual([])

        expect(phoneService.phones[3].name).toEqual('phone name2')
        expect(phoneService.phones[3].company).toEqual('company name2')
        expect(phoneService.phones[3].price).toEqual(600)
        expect(phoneService.phones[3].components).toEqual([])

        expect(phoneService.phones.length).toBe(4, 'phones[] should have length 4')

        sub.unsubscribe()
    })
})
