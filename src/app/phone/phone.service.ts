import { Injectable } from '@angular/core'
import { AlertService } from '../alert/alert.service'
import { Router } from '@angular/router'
import { environment } from 'src/environments/environment'
import { PhoneComponent } from 'src/modules/PhoneComponent'
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http'
import { tap, catchError, map } from 'rxjs/operators'
import { AuthenticationService } from '../account/authentication.service'
import { Observable, BehaviorSubject, throwError } from 'rxjs'
import { Phone } from 'src/modules/Phone'

@Injectable({
    providedIn: 'root'
})
export class PhoneService {
    phones: Phone[] = []
    detailPhone: Phone

    constructor(
        private alertService: AlertService,
        private http: HttpClient,
        private authenticationService: AuthenticationService
    ) {}

    public createPhone(phone: Phone) {
        this.phones.push(phone)
        console.log('CreateComponent')
        console.log(`POST ${environment.apiUrl}/api/components`)
        const token = this.authenticationService.token
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: token
        })
        return this.http
            .post(`${environment.apiUrl}/api/phones`, phone, { headers })
            .pipe(tap(data => console.log(data), error => console.error(error)))
            .subscribe({
                next: (response: any) => {
                    console.log(`Phone id of created component ${response._id}`)
                    phone._id = response._id
                    this.alertService.success(`Telefoon is met de naam '${phone.name}' aangemaakt`)
                },
                error: (message: any) => {
                    console.log('error:', message)
                    this.alertService.error('Er is iets mis gegaan bij het aanmaken van de telefoon.')
                }
            })
    }

    public getPhones(): Observable<Phone[]> {
        console.log('getPhones')
        console.log(`Get ${environment.apiUrl}/api/phones`)
        return this.http.get<ApiResponse>(`${environment.apiUrl}/api/phones`).pipe(
            catchError(this.handleError),
            map(response => response.result.map(data => new Phone(data))),
            tap(phones => {
                this.phones = phones
            })
        )
    }

    public getPhone(id: string): Phone {
        console.log(`getPhone(${id})`)
        if (this.phones) {
            return this.phones.find(phone => phone._id === id)
        } else {
            return undefined
        }
    }

    public updatePhone(phone: Phone) {
        console.log('updatePhone')
        console.log(`${environment.apiUrl}/api/phones/${phone._id}`)
        const token = this.authenticationService.token
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: token
        })
        const index = this.phones.findIndex(searchedPhone => searchedPhone._id === phone._id)
        this.phones[index] = phone
        return this.http
            .put(`${environment.apiUrl}/api/phones/${phone._id}`, phone, { headers })
            .pipe(tap(data => console.log(data), error => console.error(error)))
            .subscribe({
                next: (response: any) => {
                    console.log(`Phone id of updated phone ${response._id}`)
                    this.alertService.success(`Telefoon met de naam '${phone.name}' is gewijzigd`)
                },
                error: (message: any) => {
                    console.log('error:', message)
                    this.alertService.error('Er is iets mis gegaan bij het wijzigen van de telefoon.')
                }
            })
    }

    public deletePhone(id: string) {
        console.log('DeletePhone')
        console.log(`Delete ${environment.apiUrl}/api/phones/${id}`)
        const token = this.authenticationService.token
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: token
        })
        return this.http
            .delete(`${environment.apiUrl}/api/phones/${id}`, { headers })
            .pipe(tap(data => console.log(data), error => console.error(error)))
            .subscribe({
                next: (response: any) => {
                    console.log(`Phone id of deleted phone ${response._id}`)
                    this.alertService.success(`Telefoon is verwijderd.`)
                },
                error: (message: any) => {
                    console.log('error:', message)
                    this.alertService.error('Er is iets mis gegaan bij het verwijderen van de telefoon.')
                }
            })
    }

    private handleError(error: HttpErrorResponse) {
        console.log('handleError')
        // return an observable with a component-facing error message
        return throwError(
            // 'Something bad happened; please try again later.'
            error.message || error.error.message
        )
    }
}

export interface ApiResponse {
    result: any[]
}
