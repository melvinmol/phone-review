import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
    selector: 'app-about-usecases',
    templateUrl: './usecases.component.html',
    styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
    useCases: UseCase[] = [
        {
            id: 'PR-1',
            name: 'About pagina',
            description:
                'Een gebruiker kan de about page zien, zodat een ' +
                'gebruiker weet waar de applicatie over gaat.',
            scenario: ['Klikt op de about knop in het navigatie menu.'],
            actor: 'Gebruiker',
            precondition: ['Geen'],
            postcondition: 'De gebruiker is op de about pagin.'
        },
        {
            id: 'PR-2',
            name: 'Inloggen administrator',
            description: 'Een administrator kan inloggen, zodat hij reviewers kan toevoegen.',
            scenario: ['Navigeert naar login.', 'Voert zijn gegevens in.', 'Drukt op inloggen.'],
            actor: 'Administrator',
            precondition: ['Geen'],
            postcondition: 'Administrator is ingelogd.'
        },
        {
            id: 'PR-3',
            name: 'Reviewer toevoegen',
            description:
                'Een administrator kan een account aanmaken voor een reviewer, zodat de reviewer reviews kan plaatsen.',
            scenario: ['Navigeert naar reviewer toevoegen.', 'Voert zijn gegevens in.', 'Drukt op aanmaken.'],
            actor: 'Administrator',
            precondition: ['PR-2'],
            postcondition: 'Review is aangemaakt.'
        },
        {
            id: 'PR-4',
            name: 'Telefoon toevoegen',
            description:
                'Een administrator kan een telefoon toevoegen, zodat reviewers deze telefoon kunnen reviewen.',
            scenario: [
                'Navigeert naar telefoon toevoegen.',
                'Voert de telefoon gegevens in.',
                'Voegt componenten toe aan de telefoon.',
                'Drukt op aanmaken.'
            ],
            actor: 'Administrator',
            precondition: ['PR-17'],
            postcondition: 'Telefoon is aangemaakt.'
        },
        {
            id: 'PR-5',
            name: 'Telefoon wijzigen',
            description: 'Een administrator kan een telefoon wijzigen, zodat fouten verbeterd kunnen worden.',
            scenario: [
                'Klikt op telefoon wijzigen.',
                'Verbeterd de telefoon gegevens.',
                'Drukt op aanpassen.'
            ],
            actor: 'Administrator',
            precondition: ['PR-4', 'PR-8'],
            postcondition: 'Telefoon is gewijzigd.'
        },
        {
            id: 'PR-6',
            name: 'Telefoon verwijderen',
            description:
                'Een administrator kan een telefoon verwijderen, ' +
                'zodat dubbele telefoons niet meer in het systeem voorkomen.',
            scenario: [
                'Klikt op telefoon verwijderen.',
                'Verbeterd de telefoon gegevens.',
                'Drukt op aanpassen.'
            ],
            actor: 'Administrator',
            precondition: ['PR-4', 'PR-8'],
            postcondition: 'Telefoon is verwijderd.'
        },
        {
            id: 'PR-7',
            name: 'Telefoon lijst',
            description:
                'Een gebruiker kan een lijst van telefoons zien, ' +
                'zodat hij een overzicht heeft van alle beschikbare telefoons in het systeem.',
            scenario: ['Navigeert naar de telefoonlijst.'],
            actor: 'Gebruiker',
            precondition: ['PR-4'],
            postcondition: 'Krijgt een lijst met telefoons.'
        },
        {
            id: 'PR-8',
            name: 'Telefoon details',
            description:
                'Een gebruiker kan de details van een telefoon inzien, ' +
                'zodat hij weet uit welke onderdelen deze telefoon bestaan.',
            scenario: ['Klik in de telefoon in de telefoonlijst.'],
            actor: 'Gebruiker',
            precondition: ['PR-7'],
            postcondition: 'Detail pagina van de telefoon.'
        },
        {
            id: 'PR-9',
            name: 'Inloggen Reviewer',
            description: 'Een reviewer kan inloggen, zodat hij reveiws kan plaatsen.',
            scenario: ['Navigeert naar login.', 'Voert zijn gegevens in.', 'Drukt op inloggen.'],
            actor: 'Reviewer',
            precondition: ['PR-3'],
            postcondition: 'Reviewer is ingelogd.'
        },
        {
            id: 'PR-10 --optioneel--',
            name: 'Wijzigen gegevens reviewer',
            description:
                'Een reviewer kan zijn account gegevens wijzigen, zodat er fouten verbeterd kunnen worden.',
            scenario: ['Navigeert naar account wijzigen.', 'Verbetert zijn gegevens.', 'Drukt op aanpassen.'],
            actor: 'Reviewer',
            precondition: ['PR-9'],
            postcondition: 'Gegevens reviews zijn gewijzigd.'
        },
        {
            id: 'PR-11 --optioneel--',
            name: 'Reviewer verwijderen',
            description:
                'Een reviewer kan zijn account verwijderen, ' +
                'zodat hij geen persoonlijke gegeven meer hoeft achter te laten, op het platform.',
            scenario: ['Navigeert naar account verwijderen.', 'Klikt op account verwijderen.'],
            actor: 'Reviewer',
            precondition: ['PR-9'],
            postcondition: 'Account is verwijderd.'
        },
        {
            id: 'PR-12 --optioneel--',
            name: 'Review plaatsen',
            description:
                'Een reviewer kan een review plaatsen bij een telefoon, ' +
                'zodat gebruikers zijn review kunnen lezen.',
            scenario: [
                'Klikt op review plaatsen.',
                'Voert naam van de review in.',
                'Voegt een beschrijving toe.',
                'Voegt voor en nadelen toe.',
                'Klikt op review plaatsen.'
            ],
            actor: 'Reviewer',
            precondition: ['PR-8', 'PR-9'],
            postcondition: 'Review is geplaatst.'
        },
        {
            id: 'PR-13',
            name: 'Review lijst telefoon.',
            description:
                'Een gebruiker kan een lijst van reviews zien bij een telefoon, ' +
                'zodat hij de ervaringen van de reviewers kan lezen.',
            scenario: ['Klikt op een telefoon.'],
            actor: 'Gebruiker',
            precondition: ['PR-12'],
            postcondition: 'Ziet een lijst met reviews.'
        },
        {
            id: 'PR-14 --optioneel--',
            name: 'Eigen geschreven reviews lijst.',
            description:
                'Een reviewer kan een lijst van eigen geschreven reviews inzien, ' +
                'zodat hij terug kan lezen wat hij allemaal geschreven heeft.',
            scenario: ['Klikt op eigen geschreven reviews'],
            actor: 'Reviewer',
            precondition: ['PR-12'],
            postcondition: 'Ziet een lijst met eigen geschreven reviews.'
        },
        {
            id: 'PR-15 --optioneel--',
            name: 'Review wijzigen.',
            description:
                'Een reviewer kan een eigen geschreven review wijzigen, ' +
                'zodat er fouten in de review verbeterd kunnen worden.',
            scenario: ['Klikt op wijzigen.', 'Wijzigt review.', 'Klikt op wijzigen.'],
            actor: 'Reviewer',
            precondition: ['PR-14'],
            postcondition: 'Review is gewijzigd.'
        },
        {
            id: 'PR-16 --optioneel--',
            name: 'Review verwijderen.',
            description:
                'Een reviewer kan een eigen review verwijderen, ' +
                'zodat deze niet meer zichtbaar is bij de lijst eigen geschreven reviews, ' +
                'en niet meer zichtbaar is bij de telefoon zelf.',
            scenario: ['Klikt op verwijderen.'],
            actor: 'Reviewer',
            precondition: ['PR-14'],
            postcondition: 'Review is verwijderd.'
        },
        {
            id: 'PR-17',
            name: 'Component toevoegen',
            description:
                'Een administrator kan een component toevoegen, zodat deze toegevoegd kan worden aan een telefoon.',
            scenario: [
                'Navigeert naar component toevoegen.',
                'Voert de component gegevens in.',
                'Drukt op aanmaken.'
            ],
            actor: 'Administrator',
            precondition: ['PR-2'],
            postcondition: 'Component is aangemaakt.'
        },
        {
            id: 'PR-18',
            name: 'Component lijst',
            description:
                'Een gebruiker kan alle componenten zien in een lijst, zodat weet welke componenten in het systeem staan.',
            scenario: ['Navigeert naar component lijst.'],
            actor: 'Gebruiker',
            precondition: ['PR-17'],
            postcondition: 'Component lijst.'
        },
        {
            id: 'PR-19',
            name: 'Component wijzigen',
            description:
                'Een administrator kan een component wijzigen, zodat fouten verbeterd kunnen worden.',
            scenario: [
                'Klikt op component wijzigen.',
                'Verbeterd de component gegevens.',
                'Drukt op aanpassen.'
            ],
            actor: 'Administrator',
            precondition: ['PR-18'],
            postcondition: 'Telefoon is gewijzigd.'
        },
        {
            id: 'PR-20',
            name: 'Component verwijderen',
            description:
                'Een administrator kan een component verwijderen, ' +
                'zodat dubbele component niet meer in het systeem voorkomen.',
            scenario: [
                'Klikt op component verwijderen.',
                'Verbeterd de component gegevens.',
                'Drukt op aanpassen.'
            ],
            actor: 'Administrator',
            precondition: ['PR-18'],
            postcondition: 'Component is verwijderd.'
        }
    ]

    constructor() {}

    ngOnInit() {}
}
